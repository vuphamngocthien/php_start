<?php 
    require_once("../core/App.php");
    $config = require_once('../config/config.php');
    
    App::setConfig($config);

    $app = new App;
    $app->run();
?>