<?php 
    require_once("Router.php");
    //require_once("../controllers/UserController.php");
    require_once("Autoload.php");
    use app\core\Router;
class App{

     private $router; 
     public static $config;
    function __construct()
    {
        new Autoload;
        new php\controllers\UserController;
        $this->router = new Router;
        $this->router->get('/city','UserController@getAllCities');
        $this->router->get('/user','UserController@getAllUser');
        $this->router->post('/user/login','UserController@Login');
        $this->router->post('/user/update/{id}','UserController@updateUser');
        $this->router->post('/user/insert','UserController@insertUser');
        $this->router->delete('/user/delete/{id}','UserController@deleteUser');
        $this->router->get('/user/show/{id}','UserController@showUser');
        

        $this->router->any('*',function(){
            echo "404 not found";
        });
    }
    public static function setConfig($config)
    {
        self::$config = $config;
    }
    public static function getConfig()
    {
        return  self::$config;
    }
   public function run()
    {
        $this->router->run();
       
    }
}

?>