<?php 
    namespace app\core;
    class Router
    {
        private $router = [];
        function __construct()
        {

        }
        private function getRequestURL()
        {
            $basePath = \App::getConfig()['basePath'];
            $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';//lay duong dan url khong request
            $url = str_replace($basePath,"",$url);
          
            $url = $url === '' || empty($url) ? '/' : $url;
            

           return $url;
        }
        private function getRequestMethod()
        {
            $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';// lay phuong thuc
            return $method;
        }
        private function addRouter($method,$url,$action)
        {
            $this->router[] = [$method,$url,$action];
        }

        public function get($url,$action)
        {
            $this->addRouter('GET',$url,$action);
        }
        public function post($url,$action)
        {
            $this->addRouter('POST',$url,$action);
        }
        public function any($url,$action)
        {
            $this->addRouter('GET|POST',$url,$action);
        }
        public function delete($url,$action)
        {
            $this->addRouter('DELETE',$url,$action);
        }

        public function map()
        {
            $checkroute = false;
            $requestURL = $this->getRequestURL();
            $requestMethod = $this->getRequestMethod();
            $routers = $this->router;
            $param =[];
            foreach($routers  as $router )
            {
               list($method,$url,$action) = $router;
            //    echo $requestMethod;
            //    echo "<br/>";
            //    echo $method;
            if( strpos($method,$requestMethod) === FALSE)//check method co dung khong
            {
                continue;
            }


            if( $url == "*")//kiem tra url co khong
            {
                $checkroute = true;
            }else if(strpos($url,"{") === FALSE){ //kiem tra co gia tri khong
                if(strcmp(strtolower($url) , strtolower($requestURL)) === 0 )
                {
                    $checkroute = true;
                }else
                {
                    continue;
                }
               
            }else if((strpos($url,"}") === FALSE))//kiem tra co dong gia tri khong, neu khong la sai duong dan
            {
                continue;
            }else
            {
               
                $routeParams = explode('/',$url);
                $requestParams = explode('/',$requestURL);
             
                if(count($routeParams) !== count($requestParams))
                {
                    continue;
                }
                foreach($routeParams as $k => $rp)
                {
                        if(preg_match("/^{\w+}$/",$rp))
                        {
                            $param[] = $requestParams[$k];
                        }
                    
                   
                       
                  
                  
                }
               
                $checkroute = true;
          
            }
            if($checkroute === true)//kiem tra route co dung khong
            {
                if(is_callable($action))
                {
                   call_user_func_array($action, $param);
                }else if(is_string($action))
                {
                  $this->compileRoute($action,$param);
            }
                return;
            }else
            {
                continue;
            }
            }
            return;
        }
       public function compileRoute($action,$param)
        {
            if(count(explode("@",$action)) !== 2)
            {
                die('Router errol');
            }
                $classname = explode("@",$action)[0];
                $method =  explode("@",$action)[1];
                $classnamespace = "php\\controllers\\".$classname;
          
            if(class_exists($classnamespace))
            {

              $object = new $classnamespace;
              if(method_exists($classnamespace,$method))
              {
                    call_user_func_array([$object,$method],$param);
              }else
              {
                die('method not found');
              }
            }else
            {
               die("class not found".$classnamespace);
            }
           
        }
      public function run()
        {
            $this->map();
              
          
        }
        




    }


?>