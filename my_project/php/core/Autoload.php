<?php 
    class Autoload
    {
        function __construct()
        {
           spl_autoload_register([$this,'autoload']);
        }

        private function autoload($class)
        {
            $rootPath = App::getConfig()["rootPath"];
            $pathclass = explode('\\', $class);
            $className = end($pathclass);
            $pathName = str_replace($className,"",$class);
            
            $filePath = $rootPath.'\\'.$pathName.$className.".php";
       
            if(file_exists($filePath))
            {
                require_once($filePath);
            }
          
           
        
        }
       
    }


?>