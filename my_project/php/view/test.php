db = new db();
$connect = $db->connect();

$users = new Users($connect);
$getUser = $users->getUsers();

$row = $getUser->num_rows;

if($row > 0)
{
    $user_array = [];
    $user_array['data'] = [];
         while($result = $getUser->fetch_assoc()) {
            extract($result);
           $users_item = array(
        'UserId' =>   $UserId,
        'UserName' => $UserName,
        'UserPassWord' => $UserPassWord,
        'Address' => $Address,
        'UserPicture' => $UserPicture,
        'Email' => $Email,
        'Gender' =>  $Gender,
        'DateOfBirth' =>  $DateOfBirth,
        'PhoneNumber' => $PhoneNumber,
        'Created_at' => $Created_at,
        'Updated_at' =>  $Updated_at,


           );
           array_push($user_array['data'],$users_item);
          }
         return json_encode($user_array);
        } 
       
