<?php
namespace php\controllers;


require_once('../config/db.php');

use php\config\db;
use php\model\Users;


class UserController
{
   public $db;
   public $connect;

   
   function __construct()
   {
   
      $db   =  new db;
      $this->connect = $db->connect();
      
     
     
   } 
   function getAllUser()
   {
      $users = new Users($this->connect);
    
      $getUser = $users->getUsers();
     
       $this->swapArr($getUser);
   }
   function getAllCities()
   {
      
        $cities = new Users($this->connect);
      $url = 'https://dev-api-tigo.newweb.vn/v0/1/cities';
  

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($ch);
     $response = json_decode($response,true);
     
    //  echo json_encode($response);
    // echo $response["data"][0]['id'];
   //   echo sizeof( $response["data"]);
   $city = $cities->insertcity($response);
    //  echo extract($response["data"]);
      curl_close($ch);
   }
   function updateUser($id)
   {
      $users = new Users($this->connect);

      $getUser = $users->updateUser($id);
      if(is_string($getUser))
      {
         $this->swapError($getUser);
      }else
      {
         $this->showUser($id);
      }

   }
   function insertUser()
   {
      $users = new Users($this->connect);

      $getUser = $users->insertUser();

    
      if(is_string($getUser) === true)
      {
         $this->swapError($getUser);
       
      }else
      {
         $getUser = $users->showAfterInsert();
            $this->swapArr($getUser);

      }
  

   }
   function  showUser($id)
   {
      $users = new Users($this->connect);
    
      $getUser = $users->showUsers($id);

       $this->swapArr($getUser);


   }
   function  deleteUser($id)
   {
      $users = new Users($this->connect);
      $this->showUser($id);


      $getUser = $users->deleteUser($id);
  


  


   }
   function Login()
   {
      $users = new Users($this->connect);
      $UserPassWord= $_POST['user_password'];
      $passChange = md5($UserPassWord);
      

      $getUser = $users->Login();

      $row = $getUser->num_rows;
    
      $user_array = [];
      $user_array['response'] = [];
      if($row > 0)
      {
    
               while($result = $getUser->fetch_assoc()) {
                  extract($result);
                 $users_item = array(
           
              'token' => $token,
          
      
      
                 );
                 array_push($user_array['response'],$users_item);
                }
               echo json_encode($user_array);
              } else
              {
               $users_item = array(
           
                  'error' => "can't find the user ",
              
          
          
                     );
                     array_push($user_array['response'],$users_item);
                  
                 echo json_encode($user_array);
              }
              
  


  


   }
   function swapError($getUser)
   {
      $user_array = [];
      $user_array['response'] = [];
      $users_item = array(
           
         'error' => $getUser,
     
 
 
            );
            array_push($user_array['response'],$users_item);
         
        echo json_encode($user_array);
   }
   function swapArr($getUser)
   {
      $row = $getUser->num_rows;
      if($row > 0)
      {
          $user_array = [];
          $user_array['data'] = [];
               while($result = $getUser->fetch_assoc()) {
                  extract($result);
                 $users_item = array(
              'user_id' =>   $user_id,
              'user_name' => $user_name,
              'user_password' => $user_password,
              'user_address' => $user_address,
              'user_picture' => $user_picture,
              'email' => $email,
              'gender' =>  $gender,
              'dateofbirth' =>  $dateofbirth,
              'phonenumber' => $phone_number,
              'created_at' => $created_at,
              'updated_at' =>  $updated_at,
      
      
                 );
                 array_push($user_array['data'],$users_item);
                }
               echo json_encode($user_array);
              } 
   }
  

}





?>